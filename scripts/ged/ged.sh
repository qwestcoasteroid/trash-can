#!/bin/bash

[[ ! -w $PWD ]] && echo "Недостаточно прав для редактирования" && exit

CONFIG_FILE=$HOME/.config/ged.conf

[[ -z $1 ]] && echo "Необходимо ввести шаблон поиска" && exit
[[ -e $CONFIG_FILE ]] && source $CONFIG_FILE
[[ -z $GED_EDITOR ]] && GED_EDITOR=vi

NULL_OUT="1> /dev/null"
NULL_ERR="2> /dev/null"
NULL_ALL="&> /dev/null"

RAW_LINES=$(eval $NULL_ERR grep -n -i -d skip -s -I -- $1 *)

[[ -z $RAW_LINES ]] && echo "Совпадения отсутствуют" && exit
[[ -z $TEMP_FOLDER ]] && TEMP_FOLDER=$HOME/.tmp/ged
[[ ! -d $TEMP_FOLDER ]] && mkdir -p $TEMP_FOLDER
[[ ! -d $TEMP_FOLDER ]] && echo "Невозможно создать директорию временных файлов ($TEMP_FOLDER)" && exit

TEMP_FILE=$TEMP_FOLDER/tmp-$RANDOM

echo "$RAW_LINES" > $TEMP_FILE

FILES=()
LINE_NUMBERS=()

OIFS=$IFS
IFS=$'\n'

for LINE in $RAW_LINES; do
  FILES+=($(awk '{ sub(/:.*/, ""); print }' <<< $LINE))
  LINE_NUMBERS+=($(awk -F':' '{ print $2 }' <<< $LINE))
done

for FILE in ${FILES[*]}; do
  [[ ! -w $FILE ]] && echo "Файл '$FILE' недоступен для редакирования" && rm $TEMP_FILE && exit
done

INITIAL_TEMP_FILE_MODIFICATION_TIME=$(date -r $TEMP_FILE +%s)

eval $GED_EDITOR $TEMP_FILE

CURRENT_TEMP_FILE_MODIFICATION_TIME=$(date -r $TEMP_FILE +%s)

[[ $INITIAL_TEMP_FILE_MODIFICATION_TIME -eq $CURRENT_TEMP_FILE_MODIFICATION_TIME ]] && rm $TEMP_FILE && exit

RAW_LINES=$(cat $TEMP_FILE)

rm $TEMP_FILE

NEW_LINES=()

for LINE in $RAW_LINES; do
  NEW_LINES+=($(awk -F':' '{ print $3 }' <<< $LINE))
done

IFS=$' '

COUNTER=0

for FILE in ${FILES[*]}; do
  sed -i "${LINE_NUMBERS[$COUNTER]}s|.*|${NEW_LINES[$COUNTER]}|" $FILE
  ((COUNTER=COUNTER+1))
done

IFS=$OIFS
