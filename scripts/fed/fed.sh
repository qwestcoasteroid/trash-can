#!/bin/bash

CONFIG_FILE=$HOME/.config/fed.conf

[[ -e $CONFIG_FILE ]] && source $CONFIG_FILE
[[ -z $FED_EDITOR ]] && FED_EDITOR=vi

NULL_OUT="1> /dev/null"
NULL_ERR="2> /dev/null"
NULL_ALL="&> /dev/null"

FILES=$(eval $NULL_ERR ls -d -p -L *$1* | grep -v /)

[[ -z $FILES ]] && echo "Не найдено файлов, содержащих '$1'." && exit

NUMBER_OF_FILES=$(wc -w <<< $FILES)

[[ $NUMBER_OF_FILES -eq 1 ]] && eval "$FED_EDITOR $FILES" && exit

echo "Найденные файлы:"

COUNTER=1

for FILE in $FILES; do
  echo "$COUNTER) $FILE"
  ((COUNTER=COUNTER+1))
done

SELECTED_FILE=0
NUMBER_REGEXP="^[+-]?[0-9]+?$"

while [[ $SELECTED_FILE -gt $NUMBER_OF_FILES ]] || [[ $SELECTED_FILE -lt 1 ]]; do
  read -p "Введите номер нужного файла: " SELECTED_FILE
  [[ ! $SELECTED_FILE =~ $NUMBER_REGEXP ]] && SELECTED_FILE=0
done

eval "$FED_EDITOR $(awk -v number="$SELECTED_FILE" 'NR==number {print $1}' <<< $FILES)"
