# Trash Can
Всякая всячина

## scripts
Всякие скрипты
### fed
#### Описание
Производит поиск файла в текущей директории и открывает для редактирования
#### Пример использования
```shell
user@pc ~ > ls
some_file.txt another_file.txt lib.so
user@pc ~ > fed some # some_file.txt открывается для редактирования
user@pc ~ > fed file
Найденные файлы:
1) some_file.txt
2) another_file.txt
Введите номер нужного файла: 2 # another_file.txt открывается для редактирования
user@pc ~ > ls
lone_file.txt
user@pc ~ > fed # lone_file.txt открывается для редакирования
```
### ged
#### Описание
Производит поиск внутри файлов текущей директории заданного шаблона и открывает для редактирования найденные строки
##### Пример использования
```shell
user@pc ~ > ls
some_file.txt another_file.txt
user@pc ~ > cat some_file.txt
hello
world
user@pc ~ > cat another_file.txt
world
hello
user@pc ~ > ged wo # Открывается временный файл со строками формата: имя_файла:номер_строки:содержимое_строки
user@pc ~ >        # После закрытия редактора, строки из отредактированного временного файла записываются в исходные файлы
```
